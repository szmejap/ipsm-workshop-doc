# Hands-on semantics workshop – technologies and libraries

## Prerequisites

During the telco we will present how to use the prototype IPSM implementation prepared for the recent Valencia workshop. It will also serve as an introduction to implementation tasks in T3.5. To use the provided implementation you first need to download and install [Apache Kafka](kafka.md).

## IPSM prototype
The folder `bin` contains a binary "distribution" of the `IPSM` prototype and tools for generating *streams* of CSV, JSON, and XML data:

  * `bin/run-IPSM.sh` – runs the IPSM prototype (requires Apache Kafka, which is not included)
  * `bin/stop-IPSM.sh` – stops the IPSM prototype
  * `bin/run-HttpCsvSource.sh` – reactive stream of CSV values
  * `bin/run-HttpJsonSource.sh` – reactive stream of JSON values
  * `bin/run-HttpXmlSource.sh` – reactive stream of XML values

For all the "run-*" scripts above, the Kafka bootstrap server address defaults "127.0.0.1:9092". It can be changed via `--bootstrap` option, e.g.,

`bin/run-IPSM.sh --bootstrap 192.168.1.99:1234`

The `stop-IPSM.sh` script uses `REST` (via `curl`) for communication with the `IPSM`. So `curl` (or any other `HTTP` client) is required to gracefully stop `IPSM`. In the case of `curl` the command looks as follows:

`curl http://localhost:8080/terminate`


## IPSM services

IPSM REST API (execute `bin/run-IPSM.sh` and look at [http://localhost:8080/swagger/](http://localhost:8080/swagger/) for details):

| method     |    path                 |    description                |
|------------|:------------------------|:------------------------------|
| `GET`      | `/alignments`           | list allowed alignment pairs  |
| `GET`      | `/channels`             | list channels                 |
| `POST`     | `/channels`             | create channel                |
| `DELETE`   | `/channels/{channel ID}`| remove channel                |

Sample data source streams:

| format  |    port   |    URL                 |  scenario |
|---------|:----------|:-----------------------|:----------|
| `xml`   | `9999`    | `/xml[/stream_length]` |   1       |
| `json`  | `8888`    | `/json[/stream_length]`|  3,4      |
| `csv`   | `7777`    | `/csv[/stream_length]` |   2       |

## RDF

- [Primer](https://www.w3.org/TR/2014/NOTE-rdf11-primer-20140624/)
- [Tutorial](http://www.linkeddatatools.com/introducing-rdf)

### Libraries

- Java
	- [Apache Jena](https://jena.apache.org/) [Tutorial](https://jena.apache.org/tutorials/rdf_api.html)
	- [Eclipse RDF4J](http://rdf4j.org/) (aka Sesame)
- Scala
	- [Banana RDF](https://github.com/banana-rdf/banana-rdf)
- Python
	- [rdflib](https://github.com/RDFLib/rdflib)

## Reactive Streams

- [What are](http://www.reactive-streams.org/) reactive streams

### Libraries

- Java
	- [ReactiveX](http://reactivex.io/) [RxJava](https://github.com/ReactiveX/RxJava)
	- [Akka](http://akka.io/) Streams [Tutorial](http://www.lightbend.com/activator/template/akka-stream-java8)
	- [Project Reactor](https://projectreactor.io/)
- Scala
	- [ReactiveX](http://reactivex.io/) [RxScala](https://github.com/ReactiveX/RxScala)
	- [Akka](http://akka.io/) Streams [Documentation](http://doc.akka.io/docs/akka/2.4.12/general/stream/stream-design.html#stream-design), [Tutorial](http://www.lightbend.com/activator/template/akka-stream-scala)
- Python
	- [ReactiveX](http://reactivex.io/) [RxPY](https://github.com/ReactiveX/RxPY)

