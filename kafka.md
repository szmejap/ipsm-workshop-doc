## Apache Kafka standard command-line tools
Apache Kafka (even in the most recent version – `0.10.1.0` at the moment of writing this document) depends on `ZooKeeper` to function. Zookeeper is mainly used to track status of nodes present in _Kafka cluster_ and to keep track of Kafka topics, messages, etc.

For discussing the usage of standard Kafka tools we assume that Kafka has been installed/unpacked and that we are inside its main catalog, e.g., `/opt/tools/kafka`. The main catalog contains the following files and directories:

    ├── bin
    ├── config
    ├── libs
    ├── LICENSE
    ├── logs
    ├── NOTICE
    ├── site-docs
    └── target

Before starting Kafka for the first time, you need to consult and adjust its configuration file `config/server.properties`. Some interesting settings are listed below.

```nginx
# Switch to enable automatic topic creation, default value is true
#auto.create.topics.enable=false

# Switch to enable topic deletion or not, default value is false
delete.topic.enable=true

# The default number of log partitions per topic. More partitions allow greater
# parallelism for consumption, but this will also result in more files across
# the brokers.
num.partitions=1

# Zookeeper connection string (see zookeeper docs for details).
# This is a comma separated host:port pairs, each corresponding to a zk
# server. e.g. "127.0.0.1:3000,127.0.0.1:3001,127.0.0.1:3002".
# You can also append an optional chroot string to the urls to specify the
# root directory for all kafka znodes.
zookeeper.connect=localhost:2181
```



## Starting and stopping Apache Kafka
Start ZooKeeper

```bash
bin/zookeeper-server-start.sh config/zookeeper.properties
```

Start Kafka cluster

```bash
bin/kafka-server-start.sh config/server.properties
```

To stop the cluster we should first stop Kafka and only after that ZooKeeper:

```bash
bin/kafka-server-stop.sh
bin/zookeeper-server-stop.sh
```

### Topic management
List _topics_ available on Kafka cluster managed via the `ZooKeeper` on `localhost`

```bash
bin/kafka-topics.sh --zookeeper localhost --list
```

Create `stuff` topic with `pn` _partitions_ and _replication factor_ of `rf`

```bash
bin/kafka-topics.sh --zookeeper localhost --create --topic stuff --partitions pn --replication-factor rf
```

Delete `oldstuff` topic

```bash
bin/kafka-topics.sh --zookeeper localhost --delete --topic oldstuff
```

### Consuming and publishing messages
Consume messages from `stuff` topic using `localhost` as the _bootstrap server_ (mind the necessary port number)

```bash
bin/kafka-console-consumer.sh --bootstrap-server localhost:9092 --topic stuff
```

Write to `stuff` topic on _brokers_ `kafka1.com` and `kafka2.com` (even with the default port `9092`)

```bash
bin/kafka-console-producer.sh --broker-list kafka1.com:9092,kafka02.com:9092 --topic stuff
```

The data is read from `stdin` and has to end with `newline` followed by `Ctrl-d`.

## [kafkacat](https://github.com/edenhill/kafkacat)
Operates in three main _modes_: producer (`-P`), consumer (`-C`), and metadata list (`-L`).

In producer mode **kafkacat** reads messages from `stdin`, delimited with a configurable delimeter (`-D`), which defaults to `newline`, and produces them to the provided Kafka _cluster_ (specified by a list of bootstrap servers `-b`), topic (`-t`) and partition (`-p`).

Subscribe to `stuff` topic on Kafka cluster specified by a _bootstrap broker_ `localhost:9092`, with _group_ `mygroup`

```bash
kafkacat -b localhost -G mygroup stuff
```

Read messages from `stdin`, and produce them to `syslog` topic, given bootstrap broker `localhost:9092`

```bash
tail -f /var/log/syslog | kafkacat -P -b localhost -t syslog
```

Produce messages from files (`f1.rdf`, `f2.rdf`, and `f3.rdf`) to `stuff` topic (one file is one message)

```bash
kafkacat -P -b localhost -t stuff f1.rdf f2.rdf f3.rdf
```

Consume messages (from all partitions) of `stuff` topic of a Kafka cluster specified by `localhost:9092` bootstrap broker

```bash
kafkacat -C -b localhost -t stuff
```
