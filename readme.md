## What's included
The repository contains original material prepared for the "hands-on meeting" as well as a binary "distribution" of the `IPSM` prototype and tools for generating *streams* of CSV, JSON, and XML data. Further information is contained in the following documents:


  * [technologies and libraries](Technologies.md)
  * [sample workshop scenarios](Example_scenarios.md)
  
  * [Valencia WG3 outline](semantics-workshop.md)
  * [Kafka Tutorial](kafka.md)


