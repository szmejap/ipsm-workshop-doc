# Hands-on semantics workshop - example data for semantic producers/consumers

# Table of contents
1. [Introduction](#introduction)
2. [Example scenarios](#scenarios)
    1. [Scenario 1](#scenario1)
		1. [Input](#scenario1_input)
		2. [Output](#scenario1_output)
    2. [Scenario 2](#scenario2)
		1. [Input](#scenario2_input)
		2. [Output](#scenario2_output)
    3. [Scenario 3](#scenario3)
		1. [Input](#scenario3_input)
		2. [Output](#scenario3_output)
    4. [Scenario 4](#scenario4)
		1. [Input](#scenario4_input)
		2. [Output](#scenario4_output)
3. [Discussion topics](#discussion)
4. [Appendices](#appendices)
	1. [default prefix table](#prefix_tables)
	2. [Simulated alignments](#alignments)
		1. [Scenario 1](#alignments1)
		2. [Scenario 2](#alignments2)
		3. [Scenario 3](#alignments3)
		4. [Scenario 4](#alignments4)

## Introduction <a name="introduction"></a>

The objective of the implementation excercises is the creation of simple producers and consumers for semantic RDF data. These pieces of software need to accept data from an IoT artifact (platform/device) in the data format dependent on the sender, and produce data that a receiver can consume. They also need to communicate with IPSM and send/receive data in RDF format. The core functionality of producers and consumers in IPSM context is to convert content of messages between any relevant format and RDF.

From the point of view of a single platform provider, a pair consisting of one consumer and one producer is required. Assuming that the platform produces data in format `Xf`, a producer needs to accept `Xf` data, convert it to RDF and send it to IPSM. A consumer needs to accept RDF data from IPSM and convert it to `Xf`. The role of IPSM is to change the semantics of RDF data to that of the intended receiver of the message. Communication between producer/consumer and the platform is entirely up to the platform provider or owner. Note that this description mostly concerns standalone usage of IPSM i.e. without reliance on other `INTER-IoT` tools.

In short, there are two scenarios for a single participant:

Send data:

- Platform -*Xf*-> Platform semantic producer -*RDF*-> `IPSM`

Receive data:

- `IPSM` -*RDF*-> Platform semantic consumer -*Xf*-> Platform

## Example scenarios <a name="scenarios"></a>

For the implementation excercises a couple scenarios are prepared, each with its own set of input and output data - the latter for verification purposes. Each scenario example data consists of 4 files:

- 2 input files: one in the format of the sender, another in RDF
- 2 output files: one in RDF, another in the format of the sender

Each pair of files contains equivalent data.

- The input data is used by a producer. The first file (in the sender format) is an example of what the producer may receive from the platform. Its role is to then convert it to RDF format, that IPSM accepts. Provided RDF file corresponds to the first file and is an example of what the producer may send to IPSM. It exists simply to show what a good output of the producer would look like.
- The output data is used by a consumer. The first file (in RDF) is an example of what the consumer may receive from IPSM. Its role is to then convert it to the format of receiving platform. the second file corresponds to the first file and is an example of what the consumer may send to the platform. It exists simply to show what a good output of the consumer would look like.

failure to meet one or more of those restrictions may result in the message not being accepted (e.g. because of incorrect RDF), or simply not translated. Note that if you send a well formated message to a wrong channel, it will not be translated. Moreover, it will be put out a different channel than you expected.

In the final version of IPSM it will be up to the platform representative to provide explicit semantic model of the messages used in communication with the platform. If a platform sends non-conformant message (w.r.t. explicit semantics), the fault lies on its side.

**IMPORTANT:** In order to simulate communication between actual systems and avoid scenarios where one platform is sending messages to itself, the input data set represents a platform different than the one for the output data. In other words, in our examples platform 1 sends data in its own format to platform 2, which needs to receive it in another format (and semantics).

Note that the structure of the 2 input files is very similar, although the format differs (this is also true for output files). This is because any participant of an IPSM system is required to explicitly provide the ontology that its data conforms to. This is required in order to align semantics of participants with IPSM, thus enabling its core functionality. Consequently, it is the easiest for a platform to choose, or create, ontology that is a good representation of the meaning of messages that the platform already sends and receives. In other words, the ontology presented to IPSM should be a good representative for the platform semantics. This representation carries through to producers and consumers, where original messages and RDF are, ideally, identicall semantically. In short, this makes format translation much easier.

Example scenarios have streams that produce example input data associated with them. Each such (reactive) stream can, upon request, provide a stream of data in input format. The provided data is generated pseudo-randomly and is a good way to test performance and resistance of producers to different contents of input messages.

### Notes about RDF

RDF data received by IPSM is subject to some constraints:

- Each RDF entity needs to be properly prefixed.
- Each RDF message needs to conform to the input ontology specified when configuring the channel, on which the message is transmitted.
- Each entity that will be subjected to translation needs to be named (needs to have a nonempty `rdf:about` attribute).
	- You should pick your own unique URI as a base for any identifiers. For example, you may put your name and institution in the URI: http://pawel.sripas.org/portExampleProducer#". By convention, if you entities are of type owl:NamedIndividual, or any other type from an OWL ontology, the base URI should end in `#`.
	- In some scenarios producers will have to generate their own identifiers, which must be converted to proper URIs. It is advised to use a common URI base, and put IDs in place of URI fragments.
- At this time IPSM accepts and produces only RDF/XML serialization, which is the W3C recommendation. Support for other, less verbose (i.e. smaller in size) serializations is implemented, but not enabled for the demo.
- Any valid RDF/XML is accepted by IPSM, which means that producers can put any additional triples in the message. This can be used to provide additional metadata intended for the receiving platforms consumer, and not for the platform itself. Note that the consumer is free to do anything with the RDF from IPSM, and is under no obligation to put all (or any) data in the message to the platform.

### Scenario 1 <a name="scenario1"></a>

In this scenario we simulate communication between platforms that wish to exchange informations about position of devices. Sending platform uses XML format, and receiving platform accepts JSON.

Producer of platform 1:
- XML --> RDF (ont1)

Consumer of platform 2:
- RDF (ont4) --> JSON

#### Input <a name="scenario1_input"></a>

The input XML contains simple information about position of a device in one XML tag _GPSPosition_ encoded in a string containing two decimal numbers separated by a white space. Other tags (_name_ and _description_) are optional. One message may contain information about one or more devices.

```
<Device>
	<GPSposition>10.0 54.2</GPSposition>
	<name>NamedDevice1</name>
	<description>Gratia Plena</description>
</Device>
```

Corresponding RDF message contains named nodes that conform to the GeoRSS specification, when it comes to the format of positional data. This means that the contents of the XML tag _GPSPosition_ can be put directly inside the RDF node _georss:point_ without any modification.

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology1#NamedDevice1">
	<georss:point rdf:datatype="http://www.w3.org/2001/XMLSchema#string">10.0 54.2</georss:point>
	<ont1:hasDescription>Gratia Plena</hasDescription>
</owl:NamedIndividual>
```

#### Output <a name="scenario1_output"></a>

Upon receiving the above RDF message, IPSM converts it to different semantics and makes it available on appropriate channel, in RDF format. The format needs to conform to the _wgs84_ standard of positional data.

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology1#NamedDevice1">
	<wgs84_pos:lat rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">10.0</wgs84_pos:lat>
	<wgs84_pos:long rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">54.2</wgs84_pos:long>
	<ont1:hasDescription>Gratia Plena</hasDescription>
</owl:NamedIndividual>
```

The RDF needs to be transformed into JSON by a consumer. The JSON format is very simple, with data stored in a somewhat flat structure. JSON may contain information about one or more devices.

```
{
	"device": {
		"name": "NamedDevice1",
		"lat": "10.0",
		"long": "54.2",
		"description": "Gratia Plena"
	}
}
```

This example presents a simple case of *transformation* - a translation of RDF in a way that requires a manipulation of data values - in this case it is a simple string tokenization (or splitting). Other simple transformations include conversion of numerical measurements (temperature, distance, height) between different unit systems (imperial, SI).

### Scenario 2 <a name="scenario2"></a>
This scenario simulates communication between platforms that wish to exchange informations about position of devices. Sending platform uses a simple CSV format, and receiving platform accepts JSON.

Producer of platform 1:
- CSV --> RDF (ont2)

Consumer of platform 2:
- RDF (ont4) --> JSON

#### Input <a name="scenario2_input"></a>

The input CSV contains simple information about GPS data of a device. One message may contain information about one or more devices. First line contains headers. Parameters of a message are explained below:

| CSV     |   meaning  |  format |
|---------|:----------:|------:|
| `lat`   | latitude   | decimal  |
| `long`  | longitude  | decimal   |
| `err`   | GPS error  | positive decimal  |
| `satNum`| Number of satelites | positive integer  |
| `rem`   | Remark (comment) | string  |

Example message. Note that the first row lacks any `remark`, and in the second, the `lat` is missing. The producer will have to somehow solve the situation, where a (potentialy) required information is missing in a message from the platform.

```
lat,long,err,satNum,rem
128.0 , 777.2 , 2.5 , 5 ,
 , 60.0 , 5 , 3 , "missing latitude information"
```

Corresponding RDF message contains named nodes that conform to a specification from a custom port ontology (URI http://example.sripas.org/ontologies/port.owl# with 'my_port' prefix). Since the input data in CSV does not contain any information about device identifiers, or names, the producer needs to generate its own ID and put inside the rdf:about attribute.

```
<owl:NamedIndividual rdf:about="http://example.sripas.org/ontologies/port.owl#DevID1">
	<rdfs:comment></rdfs:comment>
	<my_port:hasGPSlatitude>128.0</my_port:hasGPSlatitude>
	<my_port:hasGPSlongitude>777.2</my_port:hasGPSlongitude>
	<my_port:hasGPSError>2.5</my_port:hasGPSError>
	<my_port:hasGPSSateliteNumber>5</my_port:hasGPSSateliteNumber>
</owl:NamedIndividual>
```

#### Output <a name="scenario2_output"></a>

Upon receiving the above RDF message, IPSM converts it to different semantics and makes it available on appropriate channel, in RDF format. The format needs to conform to the _wgs84_ standard of positional data, just like in [this](#scenario1) scenario.

```
<owl:NamedIndividual rdf:about="http://example.sripas.org/ontologies/port.owl#DevID1">
	<my_port:hasGPSSateliteNumber>5</my_port:hasGPSSateliteNumber>
	<my_port:hasGPSError>2.5</my_port:hasGPSError>
	<wgs84_pos:lat>128.0</wgs84_pos:lat>
	<wgs84_pos:long>777.2</wgs84_pos:long>
	<rdfs:comment></rdfs:comment>
</owl:NamedIndividual>
```

The RDF needs to be transformed into JSON by a consumer, according to the description in [this](#scenario1) scenario. Together with the mentioned scenario, this is a simulation of two platforms with different semantics and data formats communicating data to a third platform with its own semantics.

### Scenario 3 <a name="scenario3"></a>

In this scenario we simulate communication of two categories of information about a device: ownership and precision of observation. The input platform sends information in a JSON file with flat structure, and the output platform receives XML files with a more complicated structure.

Producer of platform 1:
- JSON --> RDF (ont3)

Consumer of platform 2:
- RDF (ont5) --> XML

#### Input <a name="scenario3_input"></a>

The input JSON contains information about ownership, precision of observation and functionalities of the device. Each value, except for precision, is a simple string that represents some internal identifier. One message may contain information about one or more devices.

```
{
	"device": {
		"name": "NamedDevice1",
		"precision": "10.0",
		"owner": "JohnDoe",
		"institution": "SRIPAS",
		"functions": ["openCloseFunction","actuatingFunction"]
	}
}
```

Corresponding RDF message contains named nodes that conform to a specification of a custom ontology.

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology3#NamedDevice1">
	<ont3:hasObservationPrecision rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">10.0</hasObservationPrecision>
	<ont3:hasOwner rdf:resource="http://www.example.org/ontology3#JohnDoe"/>
	<ont3:hasOwnerInstitution rdf:resource="http://www.example.org/ontology3#SRIPAS"/>
	<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#openCloseFunction"/>
	<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#actuatingFunction"/>
</owl:NamedIndividual>
```

#### Output <a name="scenario3_output"></a>

Upon receiving the above RDF message, IPSM converts it to a more complicated model of the receiving platform:

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology3#NamedDevice1">
	<ont5:hasOwner>
		<rdf:Description rdf:about="http://www.example.org/ontology3#JohnDoe">
			<ont5:hasInstitution>
				<owl:NamedIndividual rdf:about="http://www.example.org/ontology3#SRIPAS">
					<rdf:type rdf:resource="http://www.example.org/ontology5#Institution"/>
				</owl:NamedIndividual>
			</ont5:hasInstitution>
		</rdf:Description>
	</ont5:hasOwner>
	<ont5:observes>
		<ont5:ObservableProperty>
			<ont5:hasFrequency></ont5:hasFrequency>
			<ont5:hasPrecision rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">10.0</ont5:hasPrecision>
		</ont5:ObservableProperty>
	</ont5:observes>
</owl:NamedIndividual>
```

The structure can be presented in a simplified notation:

```
Device
|_hasOwner
	|_hasInstitution
|_observes
	|_observableProperty
		|_hasPrecision (xsd:decimal)
		|_hasFrequency (no data available)
```

This representation differs from the simpler model of the input, even though it contains corresponding information. There are three important distinctions:

- First of all, the model of receiver does not contain information about device functions, so this information will necessarily be disregarded by the consumer. In other words, device functionalities are not in the receivers vocabluary, so there is no way to inform it about them. The actual message from IPSM will contain the function information, because anything that it cannot translate is transmitted as-is. Nevertheless, it is up to the consumer to decide what to do with this additional information (e.g. put it in a "comment" field, log it as a warning, etc). Because of flexibility and extensibility of RDF it is not advised to ignore messages with extra (or even missing) content.
- The structure of information is decidedly less flat, compared to the input. 
- The output model requires any observableProperty to have frequency information associated with it. Because of this, IPSM adds the frequency proerty, but since this information is missing from the input message, its content is empty.

Receiver platform accepts messages in XML with the following format:

```
<Device id="NamedDevice1">
	<owner id="JohnDoe">
		<institution id="SRIPAS" />
	</owner>
	<observes>
		<property>
			<precision>10.0</precision>
			<frequency></frequency>
		</property>
	</observes>
</Device>
```

### Scenario 4 <a name="scenario4"></a>

In this scenario we simulate very simple communication of information about a function of a device. The input platform sends information in a JSON file with flat structure, and the output platform receives CSV files with simple values, that are mostly boolean.

Producer of platform 1:
- JSON --> RDF (ont3)

Consumer of platform 2:
- RDF (ont6) --> CSV

#### Input <a name="scenario4_input"></a>

The input JSON and RDF is in the same format as in [this](#scenario3_input) scenario.

#### Output <a name="scenario4_output"></a>

Upon receiving the above RDF message, IPSM converts it to the model of the receiving platform:

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology3#NamedDevice1">
	<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#closeFunction"/>
	<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#openFunction"/>
</owl:NamedIndividual>
```

The above excerpt presents only the part relevant to the example. Full expected output is presented below:

```
<owl:NamedIndividual rdf:about="http://www.example.org/ontology3#NamedDevice1">
	<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#closeFunction"/>
	<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#openFunction"/>
	<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#actuatingFunction"/>
	<ont3:hasObservationPrecision rdf:datatype="http://www.w3.org/2001/XMLSchema#decimal">10.0</hasObservationPrecision>
	<ont3:hasOwner rdf:resource="http://www.example.org/ontology3#JohnDoe"/>
	<ont3:hasOwnerInstitution rdf:resource="http://www.example.org/ontology3#SRIPAS"/>
</owl:NamedIndividual>
```

Notice that, unlike in this [scenario](#scenario3) most of the message was not translated. This is because the scenarios share input data, but differ when it comes to receiving platforms. In this scenario the receiving platfrom is very limited and we can only inform it about things that it explicitly understands. In this case, it means that only the function information is translated, and only when it concerns a predefined set of functionalities, defined by the receiving platforms semantics.

Receivers CSV is a simple file with a header, identifier and boolean flags:

```
ID , open , close , tag , mrmnt , timer , remark
NamedDevice1 , 1 , 1 , 1 , 0 , 1479226772 ,
NamedDevice2 , 0 , 0 , 0 , 1 , 1479226772 , "some example device"
```

| CSV     |   meaning  |  format | corresponding URI |
|---------|:----------:|:------:|:------:|
| `ID`    | device identifier   | string  | |
| `open`  | opening function  | boolean   | ont6:openFunction |
| `close`  | closing function  | boolean  | ont6:closeFunction |
| `tag`  | tag function  | boolean   | ont6:tagFunction |
| `mrmnt`  | measurement function  | boolean   | ont6:measureFunction |
| `timer`   | Current unix timestamp | integer  | |
| `rem`   | Remark (comment) | string  | |

Functions from the table below will be translated. Other functions that may exist in the input namespace do not have a translation into the output platform namespace.

| from     |   to  |
|:---------|:------|
| ont3:openCloseFunction  | ont6:openFunction , ont6:closeFunction|
| ont3:tagFunction  | ont6:tagFunction |
| ont3:measureFunction  | ont6:measureFunction |

Notice that the output CSV contains the `timer` field that is not present in RDF data. This needs to be generated by a consumer and added into the output file. In the context outside of this example scenario, this information could potentially be added by IPSM, as a "transformation" that takes no input and generates current timestamp.

### Discussion topics <a name="discussion"></a>

- Conclusions about universality of producers/consumers
	- Can we make skeleton templates for popular data formats or platforms
		- XML, JSON
		- FIWARE, OpenIoT, oneM2M, Eclipse IoT
		
- How do we integrate IPSM with other Inter-IoT modules?

- What data transformations should IPSM support?
	- Unit transformation (temperature, distance, ...)

## Appendices <a name="appendices"></a>

### Appendix 1: Default prefix table <a name="prefix_tables"></a>

| prefix  |    URI    |
|---------|:----------|
| `xml`   | http://www.w3.org/XML/1998/namespace   |
| `xsd`   | http://www.w3.org/2001/XMLSchema#   |
| `rdf`   | http://www.w3.org/1999/02/22-rdf-syntax-ns#   |
| `rdfs`   | http://www.w3.org/2000/01/rdf-schema#   |
| `owl`   | http://www.w3.org/2002/07/owl#   |
| `wgs84_pos`   | http://www.w3.org/2003/01/geo/wgs84_pos#   |
| `my_port`  | http://example.sripas.org/ontologies/port.owl#  |
| `sripas`  | http://example.sripas.org/  |
| `georss`  | http://www.georss.org/georss/  |
| `ont1`  | http://www.example.org/ontology1#  |
| `ont2`  | http://www.example.org/ontology2#  |
| `ont3`  | http://www.example.org/ontology3#  |
| `ont4`  | http://www.example.org/ontology4#  |
| `ont5`  | http://www.example.org/ontology5#  |
| `ont6`  | http://www.example.org/ontology6#  |

### Appendix 2: Simulated alignments <a name="alignments"></a>

This appendix contains unidirectional mapping cells that represent alignments correlated with example scenarios. In actuality, however, alignments in IPSM are mostly to and from a common core ontology. The actual alignments used in practice would therefore need to go through an intermediary step, which is ommited here for clarity. Even though aligning with a common core can produce at most as good of a translation as direct alignment (and at times worse), it is necessary for practical interoperability in a system with a large amount of participants.

#### Scenario 1 <a name="alignments1"></a>
```
<map>
	<Cell>
		<entity1>
			<georss:point>?x</georss:point>
		</entity1>
		<entity2>
			<wgs84_pos:lat>?y</wgs84_pos:lat>
			<wgs84_pos:long>?z</wgs84_pos:long>
		</entity2>
		<relation>=</relation>
		<measure rdf:datatype='http://www.w3.org/2001/XMLSchema#float'>1.0</measure>
		<sripas:transformation>
			<sripas:function rdf:about="&sripas;fun_string_tokenizer">
				<sripas:param name="separator">nbsp</sripas:param>
				<sripas:param name="string">?x</sripas:param>
				<sripas:return>(?y, ?z)</sripas:return>
			</sripas:function>
		</sripas:transformation>
	</Cell>
</map>
```
#### Scenario 2 <a name="alignments2"></a>
```
<map>
	<Cell>
		<entity1>
			<my_port:hasGPSlatitude>?x</my_port:hasGPSlatitude>
		</entity1>
		<entity2>
			<wgs84_pos:lat>?x</wgs84_pos:lat>
		</entity2>
		<relation>=</relation>
	</Cell>
	<Cell>
		<entity1>
			<my_port:hasGPSlongitude>?x</my_port:hasGPSlongitude>
		</entity1>
		<entity2>
			<wgs84_pos:long>?x</wgs84_pos:long>
		</entity2>
		<relation>=</relation>
	</Cell>
</map>
```
#### Scenario 3 <a name="alignments3"></a>
```
<map>
	<Cell>
		<entity1>
			<ont3:hasObservationPrecision>?x</hasObservationPrecision>
		</entity1>
		<entity2>
			<ont5:observes>
				<ont5:ObservableProperty>
					<ont5:hasFrequency></ont5:hasFrequency>
					<ont5:hasPrecision>?x</ont5:hasPrecision>
				</ont5:ObservableProperty>
			</ont5:observes>
		</entity2>
		<relation>=</relation>
	</Cell>
	<Cell>
		<entity1>
			<ont3:hasOwner>?x</ont3:hasOwner>
			<ont3:hasOwnerInstitution>?y</ont3:hasOwnerInstitution>
		</entity1>
		<entity2>
			<ont5:hasOwner>
				?x {
					<ont5:hasInstitution>
						?y {
							<rdf:type rdf:resource="&ont5;Institution"/>
						}
					</ont5:hasInstitution>
				}
			</ont5:hasOwner>
		</entity2>
		<relation>=</relation>
	</Cell>
</map>
```
#### Scenario 4 <a name="alignments4"></a>
```
<map>
	<Cell>
		<entity1>
			<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#openCloseFunction"/>
		</entity1>
		<entity2>
			<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#closeFunction"/>
			<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#openFunction"/>
		</entity2>
		<relation>=</relation>
	</Cell>
	<Cell>
		<entity1>
			<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#tagFunction"/>
		</entity1>
		<entity2>
			<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#tagFunction"/>
		</entity2>
		<relation>=</relation>
	</Cell>
	<Cell>
		<entity1>
			<ont3:hasFunction rdf:resource="http://www.example.org/ontology3#measureFunction"/>
		</entity1>
		<entity2>
			<ont6:machtBetrieb rdf:resource="http://www.example.org/ontology6#measureFunction"/>
		</entity2>
		<relation>=</relation>
	</Cell>
</map>
```
