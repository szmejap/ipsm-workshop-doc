# Hands-on semantics workshop
## Introduction
We assume that the software artifacts produced by `INTER-IoT` will (hopefully) be "loosely-coupled", to enable widespread, technology-independent utilization within the IoT community. Therefore we are not in a position to "dictate" you what technologies/tools you should use for implementing the `IPSM` _producers_/_consumers_ during the workshop. We can tell you what we are using for coding the `IPSM` itself, though :)

- `Java` interoperable [Scala](http://www.scala-lang.org/) programming language (on the `JVM 8`)
- [SBT](http://www.scala-sbt.org/) as the build tool
- [Akka](http://akka.io/), [Akka Streams](http://doc.akka.io/docs/akka/2.4.12/scala/stream/) (with [Apache Kafka](https://kafka.apache.org/) support), and [Akka Http](http://doc.akka.io/docs/akka-http/current/scala.html) for the "infrastructure"
- [banana-rdf](https://github.com/banana-rdf/banana-rdf) with [Apache Jena](https://jena.apache.org/) backend for `RDF` processing
- several standard `Scala`/`Java` libraries for various other things, like `JSON` and `XML` processing etc.
- [IntelliJ IDEA](https://www.jetbrains.com/idea/) as the IDE

As far as the "assumed knowledge" is concerned, we shall need:
- basic understanding of `RDF` (Resource Description Framework)
- understanding of `Reactive Streams` (a language independent standard, with many implementations, for many programming languages; we shall not need any deeper understanding of the reactive streams protocol, though)
- some basic understanding of `Apache Kafka`

For all the above items we suggest you to consult the materials listed at the end of this document. We also plan to give a short introduction to them at the beginning of the workshop - see the agenda below.

In programming terms, we shall need at least the following
- ability to serialize/de-serialize `JSON`, `XML`, `RDF/XML`, and `CSV`
- ability to publish/subscribe to `Apache Kafka` _topics_
- ability to consume data from `HTTP` sources

## Semantics workshop - agenda
### PART I – Introduction
1. `RDF`
  - what is it (graph, triple)
  - `RDF/XML`
2. General `IPSM` introduction
  - _producers_ and _consumers_
  - `IPSM` processing
3. Streams with `IPSM` & `Apache Kafka`
  - reactive streams (streams with back-pressure)
  - `Apache Kafka` introduction

### PART II – coding session: IPSM producers & consumers
1. What will be provided
  - sample data
    - input data examples
    - ontologies examples
    - (alignment examples)
  - working `IPSM`-like service
  - working data source(s) and sink(s) (`XML`, `CSV`, `JSON`)
2. Expected result: working simple `IPSM` producer(s) and consumer(s)

## Materials
- [Introducing RDF/XML](http://www.linkeddatatools.com/introducing-rdf-part-2) - a short tutorial on `RDF` and its `XML` serialization
- [Reactive Integrations with Akka Streams that Just Work](https://youtu.be/r3YSpH3GMlk) - a video presentation on reactive streams and `Akka` streams (mostly using `Java 8`)
- [Reactive Kafka with Akka Streams](https://youtu.be/mgVH3NMGMUg) - a video presentation on using `Apache Kafka` with reactive streams model
